function createNewUser() {
    let firstName = prompt('Enter your first name, please', 'Ivan');
    let lastName = prompt('Enter your last name, please', 'Kravchenko');
    let birthDay = prompt('Enter your birth date, please', '31.05.1988');
    let birthDaySplit = birthDay.split(".");
    let newUser = {
        firstName: firstName,
        lastName: lastName,
        birthDay: new Date(birthDaySplit[2]+'-'+birthDaySplit[1]+ '-' +birthDaySplit[0]),
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function () {
            let yearDiff = (new Date()).getFullYear() - this.birthDay.getFullYear();
            let monthDiff = (new Date()).getMonth() - this.birthDay.getMonth();
            let dayDiff = (new Date()).getDate() - this.birthDay.getDate();

            if (yearDiff == 0) {
                return yearDiff;
            }

            if (monthDiff < 0) {
                return yearDiff - 1;
            }

            if (monthDiff == 0 && dayDiff < 0) {
                return yearDiff - 1;
            }
            return yearDiff;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDay.getFullYear();
        }
    };
    return newUser;
}
let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());